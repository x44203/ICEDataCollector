import os, subprocess, time
from datetime import date
import sys

plot = False
if len(sys.argv) > 1:
    if "plot" in sys.argv:
        plot = True

if plot:
    try:
        import matplotlib.pyplot as plt
    except Exception:
        print("Please install MatPlotLib")
        exit()

    try:
        import numpy
    except Exception:
        print("Please install NumPy")
        exit()

    print("Plotting enabled")

hostname = "codeberg.org"

def get_time():
    result = subprocess.run(['ping', '-c', '1', hostname], stdout=subprocess.PIPE)
    return str(result.stdout).split('mdev = ')[1].split('/')[0]

print(get_time())

n = 0

if plot:
    plt.ion()

    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)
    ax1.set_xlabel("Seconds since unix 0")
    ax1.set_ylabel("Ping time in ms")
    #ax1.set_yscale("log")
    line1, = ax1.plot([], [], 'b.-')
    line2, = ax2.plot([], [], 'r.-')
    ax2.set_xlabel("Seconds since unix 0")
    ax2.set_ylabel("Time between pings in s")
    #ax2.set_yscale("log")
clast = time.clock_gettime(0)

with open("pinglog-" + date.today().strftime("%Y-%m-%d") + ".csv", 'a') as file:
    while True:
        n += 1
        try:
            ptime = get_time()
            ctime = time.clock_gettime(0)
            file.write(str(ctime) + ";" + str(ctime - clast) + ";" + ptime + "\n")
            print("ping " + str(n) + " time: " + str(ctime) + ";\t" + ptime)
            if plot:
                line1.set_xdata(numpy.append(line1.get_xdata(), ctime))
                line1.set_ydata(numpy.append(line1.get_ydata(), float(ptime)))
                line2.set_xdata(numpy.append(line2.get_xdata(), ctime))
                line2.set_ydata(numpy.append(line2.get_ydata(), ctime - clast))
                ax1.relim()
                ax1.autoscale_view()
                ax2.relim()
                ax2.autoscale_view()
                fig.canvas.draw()
                fig.canvas.flush_events()
            clast = ctime
        except Exception as e:
            print(e)
            pass
