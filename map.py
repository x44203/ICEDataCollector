import folium, sys, csv, os, colorsys, math, matplotlib.pyplot as plt
from library.utility import TrainType, LogType
from library.extras_processors import ExtrasProcessors



# Error checking

if len(sys.argv) < 2:
    print("Usage: map.py <filename>")
    exit()

filename = sys.argv[1]
if not os.path.exists(filename) and os.path.isfile(filename):
    print("File " + filename + " does not exist")
    exit()

if not filename.endswith(".csv"):
    print("Error: " + filename + " is not a CSV file")


with open(filename, 'r') as file:
    fileType = file.readline().replace("\n", "")
    if fileType.lower() != "gpslog 0.2":
        print("Incompatible file!")
        if len(fileType.split(";")) == 5: # Maybe automate this
            print("Please execute convert.py " + filename)
        else:
            print("File " + filename + " is not a gpslog 0.2 file but instead a " + fileType.lower() + " file")
        
        exit()
    
    mapType = LogType.GPS

    trainType = TrainType[file.readline().replace("\n", "")]
    trainDataString = file.readline().replace("\n", "")
    columns = file.readline().replace("\n", "").split(";")

    timeColumn = columns.index("time")
    latitudeColumn = columns.index("latitude")
    longitudeColumn = columns.index("longitude")
    speedColumn = columns.index("speed")

    columns.remove("time")
    columns.remove("latitude")
    columns.remove("longitude")
    columns.remove("speed")

    extraColumns = {extra: columns.index(extra) + 4 for extra in columns}



    csvreader = csv.reader(file, delimiter=';')

    if mapType == LogType.GPS:
        time = []
        coordinates = []
        stops = []
        speed = []
        extras = {extra: [] for extra in extraColumns.keys()}
        center = [0, 0]
        count = 0
        maxSpeed = 0
        lastLat = 0
        lastLon = 0
        lastHeading = 0

        for row in csvreader:
            lat = float(row[latitudeColumn])
            lon = float(row[longitudeColumn])
            dlat = abs(lat - lastLat)
            dlon = abs(lon - lastLon)

            if dlat > 0 or dlon > 0:
                heading = math.atan2(lat - lastLat, lon - lastLon)

                if len(coordinates) == 0 or abs(heading - lastHeading) < 0.1: # Direction should not change by more than 0.1 radians
                    time.append(float(row[timeColumn]))
                    coordinates.append([lat, lon])
                    speed.append(float(row[speedColumn]))

                    for extra, index in extraColumns.items():
                        extras[extra].append(row[index])

                    count += 1
                    center[0] += lat
                    center[1] += lon

                    maxSpeed = max(maxSpeed, speed[-1])

                    if(len(speed) < 2 and speed[-1] == 0):
                        stops.append(coordinates[-1])

                    #elif(speed[-1] == 0 and speed[-2] == 0 and (len(speed) == 2 or 50 > speed[-3] > 0)):
                    elif(speed[-1] < 10 and len(speed) >= 2 and 100 > speed[-2] >= 10): # Speed change less than 100 km/h from point to point, below 10 km/h mark as stop
                        stops.append(coordinates[-1])

                lastHeading = heading
            
            lastLat = lat
            lastLon = lon
        
        center[0] /= count # Average location
        center[1] /= count


        # The line style at each point
        lineStyles = ExtrasProcessors[trainType].processExtras(time, coordinates, speed, extras)


        # Reduce amount of independent lines by checking when the color or thickness changes
        lines = [] # Data format: [points, [color, tooltip, thickness]]
        coordsTemp = [] # Temporary coordinates to be added as one line
        lastLineStyle = None

        for coordinate, lineStyle in zip(coordinates, lineStyles):
            coordsTemp.append(coordinate)

            if lineStyle != lastLineStyle: # When speed changes store current path and reset temporary variables
                lines.append([coordsTemp.copy(), lineStyle])
                coordsTemp.clear()
                coordsTemp.append(coordinate)

            lastLineStyle = lineStyle


        m = folium.Map(location=center, tiles="OpenStreetMap", zoom_start=9) # Center map at center of path

        for line in lines:
            color = line[1][0]
            colorString = '#'
            if len(color) == 4:
                colorString += hex(0x100 + int(color[3] * 255))[3:5] # A
            colorString += hex(0x100 + int(color[0] * 255))[3:5] # R
            colorString += hex(0x100 + int(color[1] * 255))[3:5] # G
            colorString += hex(0x100 + int(color[2] * 255))[3:5] # B

            folium.vector_layers.PolyLine(line[0], color=colorString, tooltip=line[1][1], weight=line[1][2]).add_to(m)

        # Add stops as markers
        for stop in stops:
            folium.vector_layers.Marker(stop, tooltip = "Lat: {:.7f} Lon: {:.7f}".format(stop[0], stop[1])).add_to(m)

        

        # Make the legend
        legendString = ExtrasProcessors[trainType].makeLegend(time, coordinates, speed, extras)

        # Copied from StackOverflow and somewhat modified https://stackoverflow.com/questions/37466683/create-a-legend-on-a-folium-map
        legendHtml = f"""
        <div style="
        position: fixed; 
        bottom: 50px; left: 50px; width: 1in; height: 1.2in; 
        border:2px solid grey; z-index:9999; 
        
        background-color:white;
        opacity: 1.0;
        
        font-size:14px;
        font-weight: bold;
        
        ">
        &nbsp; 
        
        {legendString}

        </div> """ # Directly include legend

        m.get_root().html.add_child(folium.Element(legendHtml))

        # Replace .csv with .html
        m.save(filename[:-4] + '.html')

    elif mapType == LogType.GPS:
        print("Ping logs are not yet supported")