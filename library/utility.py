from enum import Enum

# Only include these in wildcard imports
__all__ = ["LogType", "TrainType", "URLs"]

class LogType(Enum):
    PING = 1
    GPS = 2

# List of train types
# To add a new train:
# - Add it as an entry to this enumeration
# - Add its URL to the 'urls' dictionary
# - Add a DataMapping with all JSON tags to data_mappings.py
# - Add an ExtrasProcessor to extras_processors.py to render it (logging works without it)
class TrainType(Enum):
    NONE = 0
    CD = 1
    ICE = 2
    RB = 3
    TEST = 4


# URLs for each train type
URLs = {
    TrainType.CD: "http://cdwifi.cz/portal/api/vehicle/realtime",
    TrainType.ICE: "https://iceportal.de/api1/rs/status",
    TrainType.RB: "https://www.wifi-bahn.de/schedule.jason",
    TrainType.TEST: "file:///tmp/schedule.jason.json"
    }