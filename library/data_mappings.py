import urllib.request, urllib.error, json
from .utility import TrainType, URLs

__all__ = ["DataMappings"]

# Maps JSON tags to measurements
class DataMapping:
    train = TrainType.NONE
    latitude = None
    longitude = None
    speed = None
    extras = []
    trainData = []

    # Returns true when in train
    @classmethod
    def checkJSON(cls):
        try:
            return urllib.request.urlopen(URLs[cls.train]).read().decode("utf-8")[0] == '{'
        except urllib.error.URLError:
            return False

    # Returns train name
    @classmethod
    def getTrainName(cls, trainData):
        return cls.train.name

# Dictionary of data mappings
DataMappings = {}

# Decorator to add class to dataMaps
def addToDataMappings(cls):
    DataMappings[cls.train] = cls
    return cls


# Data mappings for ICE, CD and TEST (copy of CD)
@addToDataMappings
class ICEDataMapping(DataMapping):
    train = TrainType.ICE # Set the train type to ICE
    latitude = "latitude" # Specify to use JSON tag 'latitude' to extract latitude information
    longitude = "longitude"
    speed = "speed"
    # Subtags can be specified like "tag.subtag". If one of them is a list, then all sub-values will be added. Only one of the data points in the path should be a list.
    extras = ["internet", "gpsStatus"] # Specify that the tag 'internet' should also be logged if it exists
    trainData = ["trainType", "tzn"] # Log this once at start of data acquisition since these values don't change

    @classmethod
    def getTrainName(cls, trainData):
        return trainData["tzn"]

@addToDataMappings
class CDDataMapping(DataMapping):
    train = TrainType.CD
    latitude = "gpsLat"
    longitude = "gpsLng"
    speed = "speed"
    extras = ["delay", "altitude", "temperature"]

@addToDataMappings
class RBDataMapping(DataMapping):
    train = TrainType.RB
    latitude = "lat"
    longitude = "lng"
    speed = "speed"
    extras = ["stations.delayedApproach", "stations.delayedDeparture", "stations.distance", "stations.track", "cards.signal"]
    trainData = ["stations.shortTag", "number", "name", "baptizerVersion", "stations.shortTag", "cards.sim", "cards.type", "cards.provider"]

    @classmethod
    def checkJSON(cls):
        try:
            dat = urllib.request.urlopen(URLs[cls.train]).read()
            return dat.decode("utf-8")[0] == '{' and json.loads(dat)["type"] == "RB"
        except urllib.error.URLError:
            return False

    @classmethod
    def getTrainName(cls, trainData):
        return (trainData["name"] + "-" + trainData["number"]).replace(" ", "-")

@addToDataMappings
class TestDataMapping(RBDataMapping):
    train = TrainType.TEST