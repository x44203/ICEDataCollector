import colorsys
import matplotlib.pyplot as plt
from .utility import TrainType
from io import StringIO

__all__ = ["ExtrasProcessors"]

# Processes extras
class ExtrasProcessor:
    train = TrainType.NONE
    
    # Arguments: time and speed are List(float), coordinates is List([float, float]) and extras is Dictionary(string, List(string))
    # Should return List([[R, G, B<, A>], tooltip, weight])
    @classmethod
    def processExtras(cls, time, coordinates, speeds, extras):
        raise(NotImplementedError)
    
    # Arguments: time and speed are List(float), coordinates is List([float, float]) and extras is Dictionary(string, List(string))
    # Should return a string to be embedded in the HTML
    @classmethod
    def makeLegend(cls, time, coordinates, speeds, extras):
        raise(NotImplementedError)

# Dictionary of data mappings
ExtrasProcessors = {}

# Decorator to add class to extrasProcessors
def addToExtrasProcessors(cls):
    ExtrasProcessors[cls.train] = cls
    return cls


# Data mappings for ICE, CD and TEST (copy of CD)
@addToExtrasProcessors
class ICEExtrasProcessor(ExtrasProcessor):
    train = TrainType.ICE # Set the train type to ICE
    internetLUT = {"HIGH": 3, "MIDDLE": 2, "LOW": 1, "NONE": 0} # Internet connectivity look-up table

    @classmethod
    def processExtras(cls, time, coordinates, speeds, extras):
        maxSpeed = 0

        for speed in speeds:
            maxSpeed = max(speed, maxSpeed)
        
        result = []

        for speed, internet in zip(speeds, extras["internet"]):
            result.append([
                colorsys.hsv_to_rgb(speed / maxSpeed * 0.8, 1, 1),
                str(speed) + " km/h",
                cls.internetLUT[internet] * 2 + 1
                ])

        return result
        
    @classmethod
    def makeLegend(cls, time, coordinates, speeds, extras):
        legendArray = [
            [colorsys.hsv_to_rgb(i / 255, 1, 1)]
            for i in range(int(255 * 0.8) + 1)]

        maxSpeed = 0

        for speed in speeds:
            maxSpeed = max(speed, maxSpeed)

        plt.rcParams["figure.figsize"] = (1.5, 1) # 1.5 x 1 inch (aaah, why is it inch...)
        plt.imshow(legendArray, extent = (0, maxSpeed / 10, 0, maxSpeed), origin = "lower") # Aspect ratio 1:10
        plt.xticks([]) # Disable X axis
        yticks = []

        for i in range(0, int(maxSpeed) - 20, 50): # Matplotlib default doesnt include maximum value, add points if they are more than 20 smaller than maxSpeed to prevent overlap
            yticks.append(i)

        yticks.append(maxSpeed)
        plt.yticks(yticks)
        plt.ylabel("km/h")

        buffer = StringIO() # Save to buffer instead of file
        plt.savefig(buffer, format = "svg", bbox_inches = "tight") # Save SVG and cull corners
        return buffer.getvalue()


@addToExtrasProcessors
class CDExtrasProcessor(ICEExtrasProcessor):
    train = TrainType.CD

    @classmethod
    def processExtras(cls, time, coordinates, speeds, extras):
        altitudes = [float(altitude) for altitude in extras["altitude"]]

        maxSpeed = 0
        minAltitude = 1e9
        maxAltitude = -1e9

        for speed in speeds:
            maxSpeed = max(speed, maxSpeed)

        for altitude in altitudes:
            minAltitude = min(altitude, minAltitude)
            maxAltitude = max(altitude, maxAltitude)
        
        # Similar to how it was done in ICEExtrasProcessor but with List Comprehension
        return [[
                colorsys.hsv_to_rgb(speed / maxSpeed * 0.8, 1, 1),
                f"{altitude:.2f} m",
                (altitude - minAltitude) / (maxAltitude - minAltitude) * 4 + 1
                ] for speed, altitude in zip(speeds, altitudes)]


@addToExtrasProcessors
class TestDataMapping(ExtrasProcessor):
    train = TrainType.TEST

    @classmethod
    def processExtras(cls, time, coordinates, speeds, extras):
        raise(NotImplementedError)