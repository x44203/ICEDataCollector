import urllib.request, urllib.error, json, time, os.path, sys, copy
from datetime import date
from enum import Enum
from library.utility import TrainType, URLs
from library.data_mappings import DataMappings

#
# USAGE: gpslog.py [plot] [delay 13.2] [CD, ICE, TEST]
# plot: Live plot
# delay: Delay between requests in seconds
# CD, ICE, TEST: Train type
#



# Whether to do a live plot with matplotlib
plot = False
trainType = TrainType.NONE

# Turn all arguments to lowercase
args = [item.lower() for item in sys.argv]

delay = 0.25 # Delay in seconds between two requests
customDelay = False # Whether custom delay is set

if len(sys.argv) > 1:
    if "plot" in args:
        plot = True
    
    if "delay" in args:
        delayIndex = args.index("delay") + 1
        if len(args) > delayIndex:
            try:
                delay = float(args[delayIndex])
                customDelay = True
            except ValueError:
                print("Invalid delay: " + args[delayIndex] + " is not a float")
                exit()
        else:
            print("Invalid delay: no delay specified")
            exit()

    
    for train in TrainType:
        if train.name.lower() in args:
            trainType = train

if plot:
    try:
        import matplotlib.pyplot as plt
    except Exception:
        print("Please install MatPlotLib")
        exit()

    try:
        import numpy
    except Exception:
        print("Please install NumPy")
        exit()

    print("Plotting enabled")


#
# Find train type
#

# If not already configured
if trainType == TrainType.NONE:
    print("Finding train type...")

    for train in URLs.keys():
        if DataMappings[train].checkJSON():
            trainType = train
            break

if trainType == TrainType.NONE:
    valid = "" # Make a list of valid train names
    for train in TrainType:
        if train != TrainType.NONE:
            valid += train.name + ", "

    print("Train type could not be determined. Please execute 'gpslog.py <train>' where '<train>' is " + valid[:-2] + " to choose train type")
    exit()

print("Train type: " + trainType.name)

mapping = DataMappings[trainType]



def getResponse(url):
    operUrl = urllib.request.urlopen(url)
    if(operUrl.getcode() == 200 or (operUrl.getcode() == None and trainType == TrainType.TEST)):
        data = operUrl.read()
        jsonData = json.loads(data)
    else:
        print("Error receiving data", operUrl.getcode())

    return jsonData # This errors out if the else branch above gets executed, is needed later



def getExtras(extrasNames, json):
    extras = {}
    for extra in extrasNames:
        if "." in extra:
            try:
                parts = extra.split(".")
                current = [copy.deepcopy(json)]

                for sub in parts:
                    if type(current[0][sub]) == list:
                        current = current[0][sub]
                    else:
                        for i in range(len(current)):
                            current[i] = current[i][sub]
                
                result = ""
                for cur in current:
                    result += str(cur) + ","

                result = result[:-1]

            except Exception as e:
                print(e)
                result = None

            extras[extra] = result

        else:
            if extra in json:
                extras[extra] = json[extra]
            else:
                extras[extra] = None

    return extras



def getStatus():
    json = getResponse(URLs[trainType])

    def getJSON(value):
        if value in json:
            return json[value]
        else:
            return None
    
    latitude  = getJSON(mapping.latitude)
    longitude = getJSON(mapping.longitude)
    speed     = getJSON(mapping.speed)
    
    extras = getExtras(mapping.extras, json)
    
    dataAvailable = latitude != None and longitude != None # Check that lat and lon data is available
    
    return dataAvailable, latitude, longitude, speed, extras



def getTrainData():
    json = getResponse(URLs[trainType])
    
    return getExtras(mapping.trainData, json)
    


trainData = getTrainData()
trainDataString = ""

for k, v in trainData.items():
    trainDataString += str(k) + ":" + str(v) + ";"

trainDataString = trainDataString[:-1]

print("Data: ", end="")
print(getStatus())
print("Train Data: ", end="")
print(trainDataString)



#
# File processing
#
# File structure:
# gpslog 0.2
# <train name>
# <train data>
# time;longitude;latitude;speed;<extra 1>;<extra 2>;...
# 123123.4321;12.345;45.678;123;None;HIGH
#     ⋮     ;   ⋮  ;  ⋮  ; ⋮; ⋮  ; ⋮

filename = "gpslog-" + date.today().strftime("%Y-%m-%d-") + trainType.name + "-" + mapping.getTrainName(trainData) + ".csv"

if os.path.isfile(filename):
    with open(filename, 'r') as file:

        # Get the file type, this should be 'gpslog'
        fileType = file.readline().replace("\n", "")
        if fileType.lower() != "gpslog 0.2":
            # TODO: More elegant solution
            print("File " + filename + " is not a gpslog 0.2 file but instead a " + fileType.lower() + " file, please run 'python converter.py " + filename + "' first")
            exit()
        
        train = file.readline().replace("\n", "")
        if train != trainType.name:
            print("File " + filename + " is for the wrong train type; " + train + " instead of " + trainType.name)
            exit()
        
        trainDataLoad = file.readline().replace("\n", "")
        if trainDataLoad != trainDataString:
            # TODO: More elegant solution
            print("File " + filename + " is for the wrong train; " + trainDataLoad + " instead of " + trainDataString + "; to resolve please rename " + filename + " to something else")
            exit()

        values = "time;latitude;longitude;speed"
        for extra in mapping.extras:
            values += ";" + extra

        fileValues = file.readline().replace("\n", "")
        if fileValues != values:
            print("File " + filename + " has the wrong arguments; expected: " + values + " got: " + fileValues)
            exit()

else:
    with open(filename, 'w') as file:
        values = "time;latitude;longitude;speed"
        
        for extra in mapping.extras:
            values += ";" + extra

        file.write("gpslog 0.2\n")
        file.write(trainType.name + "\n")
        file.write(trainDataString + "\n")
        file.write(values + "\n")



if plot: # Turn the plot on
    plt.ion()

    fig = plt.figure()
    ax1 = fig.add_subplot(221)
    ax2 = fig.add_subplot(223)
    ax3 = fig.add_subplot(122)
    ax1.set_xlabel("Seconds since unix 0")
    ax1.set_ylabel("Latitude")
    ax2.set_xlabel("Seconds since unix 0")
    ax2.set_ylabel("Longitude")
    ax3.set_xlabel("Seconds since unix 0")
    ax3.set_ylabel("Speed in km/h")
    line1, = ax1.plot([], [], 'b.-')
    line2, = ax2.plot([], [], 'r.-')
    line3, = ax3.plot([], [], 'r.-')

lastLatitude = None
lastLongitude = None
lastSpeed = None
lastExtras = None

n = 0
lastChange = time.clock_gettime(0) # Last time data has changed

with open(filename, 'a') as file:
    while True:
        n += 1
        time.sleep(delay)
        try:
            status, latitude, longitude, speed, extras = getStatus()
        except Exception as e:
            print(e)
            pass

        if status:
            try:
                ctime = time.clock_gettime(0)

                if latitude != lastLatitude or longitude != lastLongitude or speed != lastSpeed or extras != lastExtras:

                    extraValues = "" # Put all extra values into a CSV string
                    for _, v in extras.items():
                        extraValues += ";" + str(v)

                    file.write(str(ctime) + ";" + str(latitude) + ";" + str(longitude) + ";" + str(speed) + extraValues + "\n")
                    print("\n" + str(ctime) + ";" + str(latitude) + ";" + str(longitude) + ";" + str(speed) + extraValues, end="")
                    
                    if not customDelay:
                        delay = (ctime - lastChange) / 3

                    lastChange = ctime

                print("\nn = " + str(n), end="\r\u008D")
	            

                # Update last values for next time
                lastLatitude = latitude
                lastLongitude = longitude
                lastSpeed = speed
                lastExtras = extras

                if plot:
                    line1.set_xdata(numpy.append(line1.get_xdata(), ctime))
                    line1.set_ydata(numpy.append(line1.get_ydata(), float(latitude)))
                    line2.set_xdata(numpy.append(line2.get_xdata(), ctime))
                    line2.set_ydata(numpy.append(line2.get_ydata(), float(longitude)))
                    line3.set_xdata(numpy.append(line3.get_xdata(), ctime))
                    line3.set_ydata(numpy.append(line3.get_ydata(), float(speed)))
                    ax1.relim()
                    ax1.autoscale_view()
                    ax2.relim()
                    ax2.autoscale_view()
                    ax3.relim()
                    ax3.autoscale_view()
                    fig.canvas.draw()
                    fig.canvas.flush_events()
                clast = ctime
            except Exception as e:
                print(e)
                pass
