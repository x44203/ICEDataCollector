import sys

if len(sys.argv) < 2:
    print("Usage: converter.py gpslog1.csv gpslog2.csv...")

def convert(path):
    with open(path, "r") as inFile:
        inData = inFile.read()

    if inData.startswith("gpslog 0.1"):
        print("Detected gpslog 0.1")

        dataArray = inData.split("\n")

        if dataArray[1] == "RB":
            print("Error: Conversion for this train type not yet supported")

        elif dataArray[1] == "ICE":
            dataArray[2] += ";gpsStatus"
            for i in range(3, len(dataArray)):
                dataArray[i] += ";VALID"

        else:
            dataArray = ["gpslog 0.2"] + dataArray[1:2] + [""] + dataArray[2:]

            with open(path, "w") as outFile:
                outFile.write("\n".join(dataArray))


    elif "gpslog" in path.lower():
        print("Detected gpslog 0.0")

        with open(path[:-4] + "-ICE.csv", "w") as outFile:
            outFile.write("gpslog 0.2\n")
            outFile.write("ICE\n")
            outFile.write("\n")
            outFile.write("time;latitude;longitude;speed;internet\n")
            outFile.write(inData)



for arg in sys.argv[1:]:
    convert(arg)