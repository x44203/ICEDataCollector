pinglog.py logs ping and gpslog.py logs position and speed and internet connectivity.
Requires you to be on a WifiOnICE

Execute with `plot` as argument to show live plot

map.py plots recorded data, takes log file as argument, currently only gpslog.
Color is speed with red = 0 and violet = maximum speed.
Line thickness indicates internet connectivity.
Stops are marked on the map.
Map is a self-contained HTML (though leafletjs loads external resources from openstreetmap.org, jsdeliver.net, jquery.com, cloudflare.com and bootstrapcdn.com)
[Example map](https://x44203.codeberg.page/gpslog-example.html)

See library/utility.py for adding new trains

Much thanks to [f2k1de](https://codeberg.org/f2k1de) for providing me with JSON data, gps logs and testing it!
